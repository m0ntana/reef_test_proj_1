import logging
import requests
import urllib

class Hubstaff_API:
    BASE_URL = "https://mutator.reef.pl"
    
    def __init__(self, config):
        self.log = logging.getLogger("hubstaffapi")
        
        self.config = config
        self._read_config()
    
    """
      PUBLIC methods go below
    """
    
    def signin(self):
        path = "v135/account/signin"
        
        data = {
            "email": self.auth_email,
            "password": self.auth_passwd,
        }
        
        r = self._POST(path, data)
        if "auth_token" in r:
            self.auth_token = r["auth_token"]
        
        return self.is_authenticated()
    
    def activity(self, proj_id, date):
        path = f"/v135/subprojects/{proj_id}/actions/day"
        headers = {"DateStart": date}
        qs = {"date[stop]": date}
        
        r = self._GET(path, qs=qs, headers=headers)
        if self._handle_error(r):
            return []
        
        return r["daily_activities"]
    
    def org_list(self):
        r = self._GET("/v135/organization")
        if self._handle_error(r):
            return []
        
        return r["organizations"]
    
    def proj_list(self, org_id):
        r = self._GET(f"/v135/organization/{org_id}/subprojects")
        if self._handle_error(r):
            return []
        
        return r["projects"]
    
    def is_authenticated(self):
        return (self.auth_token is not None)
    
    """
      end of PUBLIC methods
    """
    
    
    """
      PRIVATE methods go below
    """
    
    """
      check if there is error on API response 
      return:
        True - if error occured
        False - if everything is good
    """
    def _handle_error(self, reply):
        if reply is None:
            self.log.error("no reply received")
            return True
            
        if "code" in reply and "error" in reply:
            self.log.error("[{}]: {}".format(reply["code"], reply["error"]))
            return True
        return False
    
    
    def _headers(self, extra={}):
        """
          have no clue which python version will be used
          just bulletproof dicts merging
        """
        headers ={
            "AppToken": self.auth_app_token
        }
        
        for key in extra:
            headers[key] = extra[key]
        
        return headers
    
    """
      does POST HTTP request
    """
    def _POST(self, path, idata = {}):
        try:
            r = requests.post(self._url(path), data = idata, headers = self._headers())
            return r.json()
        except Exception as e:
            self.log.error("_POST(): {}".format(str(e)))
            return None
    
    """
      does GET HTTP request
    """
    def _GET(self, path, qs={}, headers={}):
        try:
            r = requests.get(self._url(path, qs), headers=self._headers(headers))
            return r.json()
        except Exception as e:
            self.log.error("_GET(): {}".format(str(e)))
    
    def _url(self, path, qs = {}):
        # this type of string formatting is better readable in this case
        url = "{}/{}".format(self.BASE_URL, path)
        
        if self.auth_token:
            qs["auth_token"] = self.auth_token
        
        url = "{}?{}".format(url, urllib.parse.urlencode(qs))
        
        self.log.info(url)
        
        return url
    
    def _config_value(self, key):
        return self.config[key] if key in self.config else None
    
    def _read_config(self):
        self.auth_app_token = self._config_value("app_token")
        self.auth_email = self._config_value("email")
        self.auth_passwd = self._config_value("password")
        
        """
          set it to None so we know we weren't authenticated
        """
        self.auth_token = None