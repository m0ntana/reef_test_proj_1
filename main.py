from hubstaff_api import Hubstaff_API
from datetime import datetime, timedelta
import time
import sys
import logging
import configparser
import pandas

class Report:
    def __init__(self):
        self.log = logging.getLogger("report")
        
        """
          TREE format is:
          {
            USER_ID_1: {PROJECT_ID_1: SECONDS},
            USER_ID_2: {PROJECT_ID_2: SECONDS},
            ...
          }
        """
        
        self.tree = {}
    
    def set_activity(self, activities):
        for a in activities:
            pid = "proj#{}".format(a["project_id"])
            uid = "user#{}".format(a["user_id"])
            seconds = a["tracked"]
            
            if uid not in self.tree:
                self.tree[uid] = {}
            
            if pid not in self.tree[uid]:
                self.tree[uid][pid] = 0
            
            self.tree[uid][pid] += seconds        
        
        self.log.info(self.tree)
    
    def html(self):
        df = pandas.DataFrame(self.tree)
        return df.to_html()
        
    

class Reporter:
    def __init__(self, config_filename):
        self.config = configparser.ConfigParser()
        self.config.read(config_filename)
        
        self.api = Hubstaff_API(self.config["hubstaff"])
        self.report = Report()
        
        self.log = logging.getLogger("reporter")
    
    def save_output(self, html, date):
        if "reporter" not in self.config or "output_file" not in self.config["reporter"]:
            self.log.error("define 'output_file' in 'reporter' section of config file")
            return
        
        filename = self.config["reporter"]["output_file"].replace("{DATE}", date)
        with open(filename, "w") as f:
            f.write(html)
    
    def yesterday(self):
        return str((datetime.today() - timedelta(days=1)).date())
    
    def run(self):
        date = self.yesterday()
        
        auth_ok = self.api.signin()
        self.log.info(f"auth: {auth_ok}")
        
        
        projs = []
        for org in self.api.org_list():
            projs += self.api.proj_list(org["id"])
        
        
        activities = []
        for proj in projs:
            activities += self.api.activity(proj["id"], date)
        
        self.report.set_activity(activities)
        
        html = self.report.html()
        self.save_output(html, date)
        

if __name__== "__main__" :
    logging.basicConfig(format="%(asctime)s %(name)s:%(levelname)s: %(message)s", level=logging.INFO)
    
    if len(sys.argv) < 2:
        print("{} <config.ini>".format(sys.argv[0]))
        sys.exit(1)
    
    Reporter(sys.argv[1]).run()