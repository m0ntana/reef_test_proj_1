Run `install.sh` to setup environment.

Run `reporter.sh` to start reporter. Don't forget to specify config file as a command line argument.

Use `config.ini.sample` as config example. Set your credentials and output file name and path
